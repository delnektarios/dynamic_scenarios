﻿using UnityEngine;
using System;
using System.Collections;
using Vuforia;
using System.Threading;
using ZXing;
using System.Collections.Generic;
using UnityEngine.Networking;
using ZXing.Common;

/*        /////////////////    QR detection does not work in editor    ////////////////    */
//https://answers.unity.com/questions/272232/getting-vuforia-to-work-with-zxing.html


[AddComponentMenu("System/QRScanner")]
public class genericTest : MonoBehaviour
{
    public UnityEngine.UI.RawImage rawImage;
    public Material init_material;
    public Material project_material;
    public UnityEngine.UI.Text textinfo;
    public UnityEngine.UI.Button fullscreen_button;
    public UnityEngine.UI.Button exit_fullscreen_button;
    public UnityEngine.UI.Button closeButton;

    //private data members
    private bool cameraInitialized;
    private IBarcodeReader barCodeReader;
    private Texture2D texture2D;
    private CameraDevice cameraInstance;
    private Result currentQRScannedData;
    private byte[][] scaledImage;
    private Int64 counter;
    private PIXEL_FORMAT mPixelFormat ;
    //private Thread th;

    private void ConfigureBarcodeScanner()
    {
        barCodeReader = new BarcodeReader();
        barCodeReader.ResultFound += BarCodeReader_ResultFound;
        barCodeReader.ResultPointFound += ResultPointFound;
        barCodeReader.Options = new DecodingOptions();
        barCodeReader.Options.TryHarder = true;
    }


    private void ResultPointFound(ResultPoint point) {
        Debug.Log("ResultPointFound");
    }


    private void BarCodeReader_ResultFound(Result obj)
    {
        Debug.Log("OnResultScanned");
        if (obj != null)
        {
            Debug.Log("LoadFromWeb");
           StartCoroutine(LoadFromWeb(obj.Text));
        }
    }

    void Start()
    {
        ConfigureBarcodeScanner();
        rawImage.material = init_material;

        closeButton.gameObject.SetActive(false);
        fullscreen_button.gameObject.SetActive(false);
        exit_fullscreen_button.gameObject.SetActive(false);

        var vuforia = VuforiaARController.Instance;
        vuforia.RegisterVuforiaStartedCallback(OnVuforiaStarted);
        vuforia.RegisterOnPauseCallback(OnPaused);
        vuforia.RegisterTrackablesUpdatedCallback(OnTrackablesUpdated);
        scaledImage = new byte[240][];
        rawImage.GetComponent<RectTransform>().Rotate(new Vector3(0f, 0f, 0f));
       // th = new Thread()
    }


    

    void OnTrackablesUpdated()
    {
        if (cameraInitialized)
        {
            Image cameraFrame = null;
            try
            {
                cameraFrame = CameraDevice.Instance.GetCameraImage(PIXEL_FORMAT.GRAYSCALE);
                counter++;
            }
            catch (Exception e) { Debug.LogError(e.Message); }
            if (cameraFrame == null)
            {
                Debug.Log("CameraFeed is null");
                return;
            }
            else if (counter%30==0 && cameraFrame.BufferWidth > 0 && cameraFrame.BufferHeight > 0)
            {
                Debug.Log(" Decode Frame ");
                barCodeReader.Decode(trackWithRGBLuminance(cameraFrame));
            }
        }
        if (counter == Int64.MaxValue) {
            Debug.Log("Resetting counter");
            counter = 0;
        }
    }

    
    IEnumerator QRdecode(Image camerafeed)
    {

        Debug.Log("Height is  " + camerafeed.BufferHeight + " Width is " + camerafeed.BufferWidth);
        string imageInfo = " image: \n";
        imageInfo += " size: " + camerafeed.Width+ " x " + camerafeed.Height + "\n";
        imageInfo += " bufferSize: " + camerafeed.BufferWidth + " x " + camerafeed.BufferHeight + "\n";
        imageInfo += " stride: " + camerafeed.Stride;
        Debug.Log(imageInfo);
       // byte[] image = camerafeed.Pixels;
        /* byte[][] image2d = new byte[480][];
         for (int i = 0; i < 480; i++) {
             image2d[i] = new byte[640];
             Array.Copy(image, i * 480, image2d[i],0, 640);
         } */

        yield return barCodeReader.DecodeMultiple(trackWithRGBLuminance(camerafeed));
                   
    }

    private PlanarYUVLuminanceSource trackWithPlanarLuminance(Image camerafeed) {

        PlanarYUVLuminanceSource croppedSource =   new PlanarYUVLuminanceSource(camerafeed.Pixels, camerafeed.BufferWidth, camerafeed.BufferHeight, 160, 120, 240, 320, false);
        texture2D = new Texture2D(croppedSource.Width, croppedSource.Height, TextureFormat.Alpha8, false);
        //texture2D.LoadImage(camerafeed.Pixels);
        texture2D.LoadRawTextureData(croppedSource.Matrix);
        texture2D.Apply();
        rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(texture2D.width, texture2D.height);


        rawImage.material = project_material;
        rawImage.material.SetTexture("img", texture2D);
        rawImage.texture = texture2D;
        return croppedSource;
    }

    private RGBLuminanceSource trackWithRGBLuminance(Image camerafeed) {
        string imageInfo = " Frame Properties: ";
        imageInfo += " 1. size: width = " + camerafeed.Width + " x height = " + camerafeed.Height + "\n";
        imageInfo += " 2. bufferSize: " + camerafeed.BufferWidth + " x " + camerafeed.BufferHeight + "\n";
        imageInfo += " 3. stride: " + camerafeed.Stride;
        Debug.Log(imageInfo);
        RGBLuminanceSource src = new RGBLuminanceSource(camerafeed.Pixels, camerafeed.BufferWidth, camerafeed.BufferHeight, RGBLuminanceSource.BitmapFormat.Gray8);
        Debug.Log("Attempting loading of source with RGBLuminance has width = " + src.Width + "height = " + src.Height);
        RGBLuminanceSource croppedSource;
        byte[] croppedImage;
        if (src.CropSupported)
        {
            int left = Convert.ToInt32(camerafeed.BufferWidth * 0.25);
            int top = Convert.ToInt32(camerafeed.BufferHeight * 0.25);
            int width = Convert.ToInt32(camerafeed.BufferWidth * 0.5);
            int height = Convert.ToInt32(camerafeed.BufferHeight * 0.5);
            Debug.Log("Cropping image with values left = " + left + " top = " + top + " width = " + width + " height = " + height);
            croppedSource =(RGBLuminanceSource)src.crop(left, top , width, height);
            Debug.Log("Height is " + croppedSource.Height + " Width is " + croppedSource.Width);
            croppedImage = croppedSource.Matrix;

            //LuminanceSource inverted = croppedSource.invert();

            texture2D = new Texture2D(croppedSource.Width, croppedSource.Height,  TextureFormat.Alpha8, false);
            //texture2D.LoadImage(camerafeed.Pixels);
            texture2D.LoadRawTextureData(croppedSource.Matrix);
            texture2D.Apply();
            rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(texture2D.width, texture2D.height);
            rawImage.material = project_material;
            rawImage.material.SetTexture("img",texture2D);
            rawImage.texture = texture2D;
            return croppedSource;
        }

       
        return null;
    }


        private void convertImage(Image camerafeed) {
        texture2D = new Texture2D(camerafeed.BufferWidth, camerafeed.BufferHeight, TextureFormat.RGBA32, false);
        //texture2D.LoadImage(camerafeed.Pixels);
        texture2D.LoadRawTextureData(camerafeed.Pixels);
        texture2D.Apply();
        rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(texture2D.width, texture2D.height);
        rawImage.material = project_material;
        // rawImage.material.SetTexture("img",texture2D);
        rawImage.texture = texture2D;
    }



    IEnumerator LoadFromWeb(string url)
    {
        Debug.Log("LoadFromWeb");
        UnityWebRequest wr = new UnityWebRequest(url);
        DownloadHandlerTexture texDl = new DownloadHandlerTexture(true);
        wr.downloadHandler = texDl;
        //cameraInitialized = false;
        yield return wr.SendWebRequest();
        if (!(wr.isNetworkError || wr.isHttpError))
        {
            Debug.Log("DownloadedImage");
            //cameraInitialized = false;
            texture2D = texDl.texture;
            //rawImage.uvRect = new Rect(0, 0, texture2D.width, texture2D.height);
            rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(texture2D.width, texture2D.height);
            rawImage.material = project_material;
            rawImage.texture = texture2D;
            closeButton.gameObject.SetActive(true);
            fullscreen_button.gameObject.SetActive(true);
            exit_fullscreen_button.gameObject.SetActive(true);
            // QRCode detected.
            Debug.Log(url);
            //Application.OpenURL(data.Text);      // our function to call and pass url as text
            textinfo.text = url;
            ///////////////
        }
    }

    public void close()
    {
        //rawImage.material = init_material;
        closeButton.gameObject.SetActive(false);
        fullscreen_button.gameObject.SetActive(false);
        exit_fullscreen_button.gameObject.SetActive(false);
        rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(1000, 1000);
        //rawImage.uvRect = new Rect(0, 0, 100, 100);
        rawImage.material = init_material;
        //scanRegion.gameObject.SetActive(true);
    }

    private void OnVuforiaStarted()
    {
        Debug.Log("OnVuforiaStarted");
        //CameraDevice.CameraDeviceMode.MODE_OPTIMIZE_SPEED;

        cameraInstance = CameraDevice.Instance;

/*
        if (cameraInstance.SetFrameFormat(PIXEL_FORMAT.RGBA8888, true)) {
            mPixelFormat = PIXEL_FORMAT.RGBA8888;
        } else if (cameraInstance.SetFrameFormat(PIXEL_FORMAT.RGB888, true)) {
            mPixelFormat = PIXEL_FORMAT.RGB888;
        } else if (cameraInstance.SetFrameFormat(PIXEL_FORMAT.GRAYSCALE, true)){
            mPixelFormat = PIXEL_FORMAT.GRAYSCALE;
        }*/

        cameraInstance.SetFrameFormat(PIXEL_FORMAT.GRAYSCALE, true);
        mPixelFormat = PIXEL_FORMAT.GRAYSCALE;
        Debug.Log("PIXEL FORMAT is " + mPixelFormat);
        cameraInstance.GetVideoMode(CameraDevice.CameraDeviceMode.MODE_DEFAULT);
        cameraInstance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
       // new WaitForSeconds(5f);
        cameraInitialized = true;
    }

    void OnPaused(bool paused)
    {
        Debug.Log("OnPaused");
        if (paused) cameraInitialized = false;
        if (!paused) // resumed
        {
            // Set again autofocus mode when app is resumed
            cameraInitialized = true;
            cameraInstance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        }
    }
}




