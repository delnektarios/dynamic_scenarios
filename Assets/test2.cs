﻿using UnityEngine;
using System;
using System.Collections;
using Vuforia;
using System.Threading;
using ZXing;
using ZXing.QrCode;
using ZXing.Common;

/*        /////////////////    QR detection does not work in editor    ////////////////    */
//https://answers.unity.com/questions/272232/getting-vuforia-to-work-with-zxing.html


[AddComponentMenu("System/QRScanner")]
public class test2 : MonoBehaviour
{
	private bool cameraInitialized;
	private BarcodeReader barCodeReader;
	//public AppManager camScript;

	Texture2D texture2D;
	public UnityEngine.UI.RawImage rawImage;
	public Material init_material;
	public Material project_material;
	public UnityEngine.UI.Text textinfo;


	public UnityEngine.UI.Button fullscreen_button;
	public UnityEngine.UI.Button exit_fullscreen_button;

	public UnityEngine.UI.Button closeButton;

	public UnityEngine.UI.Button scanRegion;

	void Start()
	{
		rawImage.material = init_material;

		barCodeReader = new BarcodeReader();
		StartCoroutine(InitializeCamera());

		closeButton.gameObject.SetActive(false);
		fullscreen_button.gameObject.SetActive(false);
		exit_fullscreen_button.gameObject.SetActive(false);

		scanRegion.gameObject.SetActive(true);

		//rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(100, 100);
	}

	private IEnumerator InitializeCamera()
	{
		// Waiting a little seem to avoid the Vuforia's crashes.
		yield return new WaitForSeconds(3f);

		var isFrameFormatSet = CameraDevice.Instance.SetFrameFormat(PIXEL_FORMAT.GRAYSCALE, true);

		var isFrameFormatSet1 = CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);

		var isFrameFormatSet2 = CameraDevice.Instance.GetVideoMode(CameraDevice.CameraDeviceMode.MODE_OPTIMIZE_SPEED);

		var isFrameFormatSet3 = CameraDevice.CameraDeviceMode.MODE_OPTIMIZE_SPEED;


		Debug.Log(String.Format("FormatSet : {0}", isFrameFormatSet));
		Debug.Log(String.Format("FormatSet : {0}", isFrameFormatSet1));
		Debug.Log(String.Format("FormatSet : {0}", isFrameFormatSet2));
		Debug.Log(String.Format("FormatSet : {0}", isFrameFormatSet3));


		//CameraDevice.StartTimer(new TimeSpan(0, 0, 0, 2), () => { if (zxing.IsScanning) zxing.AutoFocus(); return true; });

		// Force autofocus.
		//        var isAutoFocus = CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
		//        if (!isAutoFocus)
		//        {
		//            CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_NORMAL);
		//        }
		//        Debug.Log(String.Format("AutoFocus : {0}", isAutoFocus));
		cameraInitialized = true;
	}

	private void Update()
	{
		if (cameraInitialized)
		{
			try
			{
				var cameraFeed = CameraDevice.Instance.GetCameraImage(PIXEL_FORMAT.GRAYSCALE);
				if (cameraFeed == null)
				{
					return;
				}
				var data = barCodeReader.Decode(cameraFeed.Pixels, cameraFeed.BufferWidth/2, cameraFeed.BufferHeight/2, RGBLuminanceSource.BitmapFormat.RGB24);
				if (data != null)
				{

					closeButton.gameObject.SetActive(true);
					fullscreen_button.gameObject.SetActive(true);
					exit_fullscreen_button.gameObject.SetActive(true);

					scanRegion.gameObject.SetActive(false);


					// QRCode detected.
					Debug.Log(data.Text);
					//Application.OpenURL(data.Text);      // our function to call and pass url as text

					//my work here/
					textinfo.text = data.Text;

					WWW contentLink = new WWW(data.Text);

					while (!contentLink.isDone)
					{
						//yield return contentLink;
					}

					//rawImage.material = project_material;
					texture2D = contentLink.texture;

					//rawImage.uvRect = new Rect(0, 0, texture2D.width, texture2D.height);
					rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(texture2D.width, texture2D.height);
					rawImage.material = project_material;
					rawImage.texture = texture2D;
					///////////////

					//data = null;        // clear data

				}
				else
				{
					//Thread.Sleep(4000);
					//rawImage.material = init_material;
					Debug.Log("No QR code detected !");
				}
			}
			catch (Exception e)
			{
				Debug.LogError(e.Message);
			}
		}

	}


	public void close()
	{
		//rawImage.material = init_material;
		closeButton.gameObject.SetActive(false);
		fullscreen_button.gameObject.SetActive(false);
		exit_fullscreen_button.gameObject.SetActive(false);
		rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(1000, 1000);
		//rawImage.uvRect = new Rect(0, 0, 100, 100);
		rawImage.material = init_material;
		scanRegion.gameObject.SetActive(true);
	}
}
