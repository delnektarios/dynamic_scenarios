﻿using UnityEngine;
using System;
using System.Collections;
using Vuforia;
using System.Threading;
using ZXing;
using ZXing.QrCode;
using ZXing.Common;

/*        /////////////////    QR detection does not work in editor    ////////////////    */
//https://answers.unity.com/questions/272232/getting-vuforia-to-work-with-zxing.html


[AddComponentMenu("System/QRScanner")]
public class original : MonoBehaviour
{
	private bool cameraInitialized;
	private BarcodeReader barCodeReader;
	//public AppManager camScript;

	Texture2D texture2D;
	public UnityEngine.UI.RawImage rawImage;
	public Material init_material;
	public Material project_material;
	public UnityEngine.UI.Text textinfo;

	void Start()
	{
		rawImage.material = init_material;

		barCodeReader = new BarcodeReader();
		StartCoroutine(InitializeCamera());
	}

	private IEnumerator InitializeCamera()
	{
		// Waiting a little seem to avoid the Vuforia's crashes.
		yield return new WaitForSeconds(3f);

		var isFrameFormatSet = CameraDevice.Instance.SetFrameFormat(PIXEL_FORMAT.GRAYSCALE, true);
		Debug.Log(String.Format("FormatSet : {0}", isFrameFormatSet));

		// Force autofocus.
		//        var isAutoFocus = CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
		//        if (!isAutoFocus)
		//        {
		//            CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_NORMAL);
		//        }
		//        Debug.Log(String.Format("AutoFocus : {0}", isAutoFocus));
		cameraInitialized = true;
	}

	private void Update()
	{
		if (cameraInitialized)
		{
			try
			{
				var cameraFeed = CameraDevice.Instance.GetCameraImage(PIXEL_FORMAT.GRAYSCALE);
				if (cameraFeed == null)
				{
					return;
				}
				var data = barCodeReader.Decode(cameraFeed.Pixels, cameraFeed.BufferWidth, cameraFeed.BufferHeight, RGBLuminanceSource.BitmapFormat.RGB24);
				if (data != null)
				{
					// QRCode detected.
					Debug.Log(data.Text);
					//Application.OpenURL(data.Text);      // our function to call and pass url as text

					//my work here/
					textinfo.text = data.Text;

					WWW contentLink = new WWW(data.Text);

					while (!contentLink.isDone)
					{
					//yield return contentLink;
					}

					rawImage.material = project_material;
					texture2D = contentLink.texture;

					rawImage.texture = texture2D;
					///////////////

					data = null;        // clear data

				}
				else
				{
					Debug.Log("No QR code detected !");
				}
			}
			catch (Exception e)
			{
				Debug.LogError(e.Message);
			}
		}

	}
}

