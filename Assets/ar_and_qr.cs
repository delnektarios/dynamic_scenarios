﻿/*



using UnityEngine;
using System;
using System.Collections;
using Vuforia;
using System.Threading;
using ZXing;
using ZXing.QrCode;
using ZXing.Common;

/*  ///////////////// QR detection does not work in editor //////////////// */


/*
[AddComponentMenu("System/QRScanner")]
public class ar_and_qr : MonoBehaviour
{
    private bool cameraInitialized = false;
	private BarcodeReader barCodeReader;
    //public AppManager camScript;

    Texture2D texture2D;
    public UnityEngine.UI.RawImage rawImage;
    public Material init_material;
    public Material project_material;

    public UnityEngine.UI.Text textinfo;

    void Start()
    {
        rawImage.material = init_material;

        barCodeReader = new BarcodeReader();

		//qrCodeReader = new QRCodeReader();


        StartCoroutine(InitializeCamera());
    }

    private IEnumerator InitializeCamera()
    {
        // Waiting a little seem to avoid the Vuforia's crashes.
        yield return new WaitForSeconds(3f);

        var isFrameFormatSet = CameraDevice.Instance.SetFrameFormat(Image.PIXEL_FORMAT.GRAYSCALE, true);
        //Debug.Log(String.Format("FormatSet : {0}", isFrameFormatSet));

        // Force autofocus.
        //  var isAutoFocus = CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        //  if (!isAutoFocus)
        //  {
        //   CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_NORMAL);
        //  }
        //  Debug.Log(String.Format("AutoFocus : {0}", isAutoFocus));
        cameraInitialized = true;
	}

    void Update()
    {
		if (cameraInitialized)
		{
			try
			{
				ZXing.Result data = null;
				//Text data;
				//see how to get a smaller image just for processing here!
				var cameraFeed = CameraDevice.Instance.GetCameraImage(Image.PIXEL_FORMAT.GRAYSCALE);
				//Debug.Log("1");
				if (cameraFeed == null)
				{
					//yield return 0;
					rawImage.material = init_material;
					//Debug.Log("2");
				}
				else if(cameraFeed != null)
				{

					data = barCodeReader.Decode(cameraFeed.Pixels, cameraFeed.BufferWidth, cameraFeed.BufferHeight);
					Debug.Log(cameraFeed.BufferWidth);
					Debug.Log(cameraFeed.BufferHeight);
				}
				
				//Debug.Log("2+++");
				if (data != null)
				{
					// QRCode detected.
					Debug.Log(data.Text);


					//Application.OpenURL(data.Text);      // our function to call and pass url as text

					textinfo.text = data.Text;

					WWW contentLink = new WWW(data.Text);
					//Debug.Log("3");

					while (!contentLink.isDone) ;
					//{
					//yield return contentLink;
					//}

					rawImage.material = project_material;
					//Debug.Log("4");
					texture2D = contentLink.texture;
					//Debug.Log("5");
					rawImage.texture = texture2D;
					//Debug.Log("6");

					Thread.Sleep(1000);
					rawImage.material = init_material;


					data = null;  // clear data
				}
				else
				{
					Debug.Log("No QR code detected !");
				}
			}
			catch (Exception e)
			{
				Debug.LogError(e.Message);
			}
		}
		//cleaning up after some time!
		//yield return new WaitForSeconds(2.0f);

		//Thread.Sleep(200);
		//rawImage.material = init_material;

	}


}

*/