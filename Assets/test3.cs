﻿using UnityEngine;
using System;
using System.Collections;
using Vuforia;
using System.Threading;
using ZXing;
using ZXing.QrCode;
using ZXing.Common;
using UnityEngine.Networking;

/*        /////////////////    QR detection does not work in editor    ////////////////    */
//https://answers.unity.com/questions/272232/getting-vuforia-to-work-with-zxing.html


[AddComponentMenu("System/QRScanner")]
public class test3 : MonoBehaviour
{
    public UnityEngine.UI.RawImage rawImage;
    public Material init_material;
    public Material project_material;
    public UnityEngine.UI.Text textinfo;
    public UnityEngine.UI.Button fullscreen_button;
    public UnityEngine.UI.Button exit_fullscreen_button;
    public UnityEngine.UI.Button closeButton;

    //private data members
    private bool cameraInitialized;
    private IBarcodeReader barCodeReader;
    private Texture2D texture2D;
    private CameraDevice cameraInstance;
    private Result currentQRScannedData;

    private void ConfigureBarcodeScanner() {
        barCodeReader = new BarcodeReader();
        barCodeReader.ResultFound += BarCodeReader_ResultFound;
        barCodeReader.Options = new DecodingOptions();
        barCodeReader.Options.TryHarder = true;
        //barCodeReader.Options.AllowedLengths
    }

    private void BarCodeReader_ResultFound(Result obj)
    {
        Debug.Log("OnResultScanned");
        if (obj != null)
        {
            StartCoroutine(LoadFromWeb(obj.Text));
        }
    }

    void Start()
    {
        ConfigureBarcodeScanner();
        rawImage.material = init_material;
    
        closeButton.gameObject.SetActive(false);
        fullscreen_button.gameObject.SetActive(false);
        exit_fullscreen_button.gameObject.SetActive(false);

        var vuforia = VuforiaARController.Instance;
        vuforia.RegisterVuforiaStartedCallback(OnVuforiaStarted);
        vuforia.RegisterOnPauseCallback(OnPaused);
        VuforiaARController.Instance.RegisterTrackablesUpdatedCallback(OnTrackablesUpdated);

    }




     void OnTrackablesUpdated()
    {
        if (cameraInitialized) { 
        Image cameraFrame = null;
        try
        {
                cameraFrame = CameraDevice.Instance.GetCameraImage(PIXEL_FORMAT.GRAYSCALE);
        }
        catch (Exception e) { Debug.LogError(e.Message); }
            if (cameraFrame == null)
            {
                Debug.Log("CameraFeed is null");
                return;
            }
            else if(cameraFrame.BufferWidth>0 && cameraFrame.BufferHeight > 0) { 
                StartCoroutine(QRdecode(cameraFrame));
            }
    }
    }
    

    private IEnumerator QRdecode(Image camerafeed) {

        Debug.Log("Height is  " + camerafeed.BufferHeight + " Width is " + camerafeed.BufferWidth);
        yield return barCodeReader.Decode(camerafeed.Pixels, camerafeed.BufferWidth, camerafeed.BufferHeight, RGBLuminanceSource.BitmapFormat.Gray16); 
    }

  


    IEnumerator LoadFromWeb(string url)
    {   
        Debug.Log("LoadFromWeb");
        UnityWebRequest wr = new UnityWebRequest(url);
        DownloadHandlerTexture texDl = new DownloadHandlerTexture(true);
        wr.downloadHandler = texDl;
        //cameraInitialized = false;
        yield return wr.SendWebRequest();
        if (!(wr.isNetworkError || wr.isHttpError))
        {
            Debug.Log("DownloadedImage");
            //cameraInitialized = false;
            texture2D = texDl.texture;
            //rawImage.uvRect = new Rect(0, 0, texture2D.width, texture2D.height);
            //rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(texture2D.width, texture2D.height);
            rawImage.material = project_material;
            rawImage.texture = texture2D;
            closeButton.gameObject.SetActive(true);
            fullscreen_button.gameObject.SetActive(true);
            exit_fullscreen_button.gameObject.SetActive(true);
                // QRCode detected.
            Debug.Log(url);
                //Application.OpenURL(data.Text);      // our function to call and pass url as text
            textinfo.text = url;
            ///////////////
        }
    }

    public void close()
    {
        //rawImage.material = init_material;
        closeButton.gameObject.SetActive(false);
        fullscreen_button.gameObject.SetActive(false);
        exit_fullscreen_button.gameObject.SetActive(false);
        rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(1000, 1000);
        //rawImage.uvRect = new Rect(0, 0, 100, 100);
        rawImage.material = init_material;
        //scanRegion.gameObject.SetActive(true);
    }

    private void OnVuforiaStarted()
    {
        Debug.Log("OnVuforiaStarted");
        //CameraDevice.CameraDeviceMode.MODE_OPTIMIZE_SPEED;
       
        cameraInstance = CameraDevice.Instance;
        cameraInstance.SetFrameFormat(PIXEL_FORMAT.GRAYSCALE, true);
        cameraInstance.GetVideoMode(CameraDevice.CameraDeviceMode.MODE_OPTIMIZE_SPEED);
        cameraInstance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_NORMAL);
        cameraInitialized = true;
    }

    private void OnPaused(bool paused)
    {
        Debug.Log("OnPaused");
        if (paused) cameraInitialized = false;
        if (!paused) // resumed
        {
            // Set again autofocus mode when app is resumed
            cameraInstance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        }
    }

}
