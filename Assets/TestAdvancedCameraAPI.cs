﻿///https://library.vuforia.com/articles/Solution/Advanced-Camera-API


using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vuforia;

public class TestAdvancedCameraAPI : MonoBehaviour
{
	void Start()
	{
		VuforiaARController.Instance.RegisterVuforiaStartedCallback(OnVuforiaStarted1);
	}

	void OnVuforiaStarted1()
	{
		// Get the fields
		IEnumerable cameraFields = CameraDevice.Instance.GetCameraFields();

		// Print fields to device logs
		foreach (CameraDevice.CameraField field in cameraFields)
		{
			Debug.Log("Key: " + field.Key + "; Type: " + field.Type);
		}

		// Retrieve a specific field and print to logs
		string focusMode = "";

		CameraDevice.Instance.GetField("focus-mode", out focusMode);

		Debug.Log("FocusMode: " + focusMode);
	}
}
