﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class contentLoader : MonoBehaviour {

    Texture2D texture2D;
    string sourceURL = "http://atthattimedaniel12.weebly.com/uploads/8/3/0/1/8301729/iran-800x500-orig-2-orig-6_orig.jpg";

    //public Renderer thisRenderer;
    public RawImage rawImage;
    // Use this for initialization
    void Start () {
        StartCoroutine(LoadContent());
        //thisRenderer.material.color = Color.clear;
        //rawImage.material.color = Color.blue;
	}

    IEnumerator LoadContent()
    {
        Debug.Log("loading content.....");
        //yield return 0;
        WWW contentLink = new WWW(sourceURL);
        yield return contentLink;

        Debug.Log("content loaded!");
        //thisRenderer.material.color = Color.blue;
        //rawImage.material.color = Color.clear;
        //thisRenderer.material.mainTexture = contentLink.texture;
        //rawImage.material.mainTexture = contentLink.texture;
        texture2D = contentLink.texture;
        rawImage.texture = texture2D;

        yield return contentLink;
    }

    /*void OnGUI()
    {
        GUILayout.Label(texture2D);
    }*/

    // Update is called once per frame
    void Update () {
		
	}
}
