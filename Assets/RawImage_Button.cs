﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RawImage_Button : MonoBehaviour {

	public RawImage rawImage;
	public Button fullscreen_button;
	public Button exit_fullscreen_button;

	public Sprite fullscreen;
	public Sprite exitFullscreen;

	public float step;
	public float limit_zoom;

	float add_step;



	// Use this for initialization
	void Start () {

		fullscreen_button.image.sprite = fullscreen;
		exit_fullscreen_button.image.sprite = exitFullscreen;

		fullscreen_button.gameObject.SetActive(true);
		exit_fullscreen_button.gameObject.SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void enter_fullscreen()
	{
		//to avoid over zooming the object
		if(	Math.Abs(add_step) < Math.Abs(limit_zoom))
		{
			add_step -= step;
		}
		
		//https://forum.unity.com/threads/setting-pos-z-in-recttransform-via-scripting.270230/
		//newbutton.GetComponent<RectTransform>().localPosition = new Vector3(1,2,3);
		rawImage.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, add_step);
		//exit_fullscreen_button.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, -zoom);

		//fullscreen_button.image.sprite = fullscreen;

		//fullscreen_button.gameObject.SetActive(false);
		//exit_fullscreen_button.gameObject.SetActive(true);

	}

	public void exit_fullscreen()
	{
		if(Math.Abs(add_step) > 1)
		{
			add_step += step;
		}
		
		//rawImage.uvRect.height = 0.0f;
		rawImage.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, add_step);
		//exit_fullscreen_button.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, -1);

		//fullscreen_button.gameObject.SetActive(true);
		//exit_fullscreen_button.gameObject.SetActive(false);
	}

	//https://answers.unity.com/questions/1172061/how-to-change-image-of-button-when-clicked.html

}
