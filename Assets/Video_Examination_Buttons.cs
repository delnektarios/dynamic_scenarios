﻿using UnityEngine;

public class Video_Examination_Buttons : MonoBehaviour {

    // Use this for initialization
    public GameObject canvas_init;
    public GameObject canvas_info;
    public GameObject canvas_exam;
    public GameObject init_text;
    //public GameObject info_text;
    public GameObject exam_text;
    public GameObject canvas_video;
    private bool video = false;
    int count = 1;

    //bool isActive = true;

    //initializing target image at start
    void Start()
    {
        Init_setActive(true);
        Info_setActive(false);
        Exam_setActive(false);

        if (canvas_video != null)
        {
            video = true;
            canvas_video.SetActive(false);
        }


    }

    void Init_setActive(bool opt)
    {
        canvas_init.SetActive(opt);
        init_text.SetActive(opt);
    }

    void Info_setActive(bool opt)
    {
        canvas_info.SetActive(opt);
        //info_text.SetActive(opt);
    }
    void Exam_setActive(bool opt)
    {
        canvas_exam.SetActive(opt);
        exam_text.SetActive(opt);
    }

    void Video_setActive(bool opt)
    {
        if (canvas_video != null)
        {
            canvas_video.SetActive(opt);
        }

    }

    // Update is called once per frame
    void Update() {
        if (count == 1)
        {
            //info_text.SetActive(false);
            //canvas_init.SetActive(true);
            //canvas_exam.SetActive(false);
        }
    }

    public void Info_Button()
    {
        Init_setActive(false);
        Info_setActive(true);
        Exam_setActive(false);
    }


    public void Examination_Button()
    {
        Init_setActive(false);
        Info_setActive(false);
        Exam_setActive(true);
    }

    public void Video_Button()
    {
        Init_setActive(false);
        Info_setActive(false);
        Exam_setActive(false);
        Video_setActive(true);
    }

    public void Return_Button()
    {
        Init_setActive(true);
        Info_setActive(false);
        Exam_setActive(false);
    }

    public void changeCanvasProperty(GameObject myCanvas, bool disable)
    {
        if (!disable)
            myCanvas.SetActive(false);
        else
            myCanvas.SetActive(true);
    }

    void enableCanvas()
    {
        //CanvasA.SetActive(true);
    }
}
