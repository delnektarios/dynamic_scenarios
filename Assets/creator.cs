﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ZXing;
using ZXing.QrCode;

public class creator : MonoBehaviour {

    private WebCamTexture camTexture;
    private Rect screenRect;

    Texture2D texture2D;

    public Text info_text;

    string sourceURL = "";

    //public Renderer thisRenderer;
    public RawImage rawImage;
    // Use this for initialization

    // Use this for initialization
    void Start () {
        screenRect = new Rect(0, 0, Screen.width, Screen.height);
        camTexture = new WebCamTexture();
        camTexture.requestedHeight = Screen.height;
        camTexture.requestedWidth = Screen.width;
        if (camTexture != null)
        {
            camTexture.Play();
        }

        //StartCoroutine(LoadContent());
    }
	
	// Update is called once per frame
	void Update () {
		
	}




    void OnGUI()
    {
        // drawing the camera on screen
        GUI.DrawTexture(screenRect, camTexture);
        // do the reading — you might want to attempt to read less often than you draw on the screen for performance sake
        try
        {
            IBarcodeReader barcodeReader = new BarcodeReader();
            // decode the current frame
            var result = barcodeReader.Decode(camTexture.GetPixels32(),
              camTexture.width, camTexture.height);
            if (result != null)
            {
                Debug.Log("DECODED TEXT FROM QR: " +result.Text);
                //info_text.text = result.ToString();
                StartCoroutine(LoadContent(result.Text));
            }
        }
        catch (System.Exception ex) { Debug.LogWarning(ex.Message); }

        //project the content dynamically here
        

        //GUILayout.Label(texture2D);
        
    }
    

    IEnumerator LoadContent(string sourceLink)
    {
        /*GUI.DrawTexture(screenRect, camTexture, ScaleMode.ScaleToFit);
        try
        {
            IBarcodeReader barcodeReader = new BarcodeReader();
            // decode the current frame
            var result = barcodeReader.Decode(camTexture.GetPixels32(),
              camTexture.width, camTexture.height);
            if (result != null)
            {
                Debug.Log("DECODED TEXT FROM QR: " + result.Text);
                info_text.text = result.ToString();
                sourceURL = result.ToString();
            }
        }
        catch (System.Exception ex) { Debug.LogWarning(ex.Message); }*/

        info_text.text = sourceLink;

        Debug.Log("loading content.....");
        yield return 0;
        WWW contentLink = new WWW(sourceURL);
        yield return contentLink;

        Debug.Log("content loaded!");
        //thisRenderer.material.color = Color.blue;
        //rawImage.material.color = Color.clear;
        //thisRenderer.material.mainTexture = contentLink.texture;
        //rawImage.material.mainTexture = contentLink.texture;
        texture2D = contentLink.texture;
        rawImage.texture = texture2D;

        yield return contentLink;
    }

 /*   //here we project the content
    IEnumerator LoadContent()
    {
        yield return 0;
        WWW contentLink = new WWW(info_text.text);
        yield return contentLink;
        texture2D = contentLink.texture;
    }

*/

}

