﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;


/// <summary>
/// Unity VideoPlayer Script for Unity 5.6 (currently in beta 0b11 as of March 15, 2017)
/// Blog URL: http://justcode.me/unity2d/how-to-play-videos-on-unity-using-new-videoplayer/
/// YouTube Video Link: https://www.youtube.com/watch?v=nGA3jMBDjHk
/// StackOverflow Disscussion: http://stackoverflow.com/questions/41144054/using-new-unity-videoplayer-and-videoclip-api-to-play-video/
/// Code Contiburation: StackOverflow - Programmer
/// </summary>


public class Video_Player : MonoBehaviour
{

    public RawImage image;

    public VideoClip videoToPlay;

    private VideoPlayer videoPlayer;
    private VideoSource videoSource;

    private AudioSource audioSource;

    //https://answers.unity.com/questions/300864/how-to-stop-a-co-routine-in-c-instantly.html
    private Coroutine my_coroutine;

    //to revert upon closure to a canvas
    public GameObject canvas_init;

    // Use this for initialization
    void Start()
    {

    }

    public void Play_Video()
    {
        Application.runInBackground = true;
        my_coroutine = StartCoroutine(playVideo());
    }

    public void Stop_Video()
    {
        
        videoPlayer.Stop();

       
        audioSource.Stop();
        Application.runInBackground = false;
        StopCoroutine(my_coroutine);

        if (canvas_init != null)
        {
            canvas_init.SetActive(true);
        }
        
        
    }

    IEnumerator playVideo()
    {

        //Add VideoPlayer to the GameObject
        videoPlayer = gameObject.AddComponent<VideoPlayer>();

        //Add AudioSource
        audioSource = gameObject.AddComponent<AudioSource>();

        //Disable Play on Awake for both Video and Audio
        videoPlayer.playOnAwake = false;
        audioSource.playOnAwake = false;
        audioSource.Pause();

        //We want to play from video clip not from url

        videoPlayer.source = VideoSource.VideoClip;

        // Video clip from Url
        //https://answers.unity.com/questions/1370621/using-videoplayer-to-stream-a-video-from-a-website.html
        //videoPlayer.source = VideoSource.Url;
        //videoPlayer.url = "https://dl.dropbox.com/s/d4f4v2df1lhtz5q/DEMO%201%2027-36%281%29%281%29.mp4";


        //Set Audio Output to AudioSource
        videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

        //Assign the Audio from Video to AudioSource to be played
        videoPlayer.controlledAudioTrackCount = 1;
        videoPlayer.EnableAudioTrack(0, true);
        videoPlayer.SetTargetAudioSource(0, audioSource);

        //Set video To Play then prepare Audio to prevent Buffering
        videoPlayer.clip = videoToPlay;
        videoPlayer.Prepare();

        //WaitForSeconds waitTime = new WaitForSeconds(5);
        //Wait until video is prepared
        while (!videoPlayer.isPrepared)
        {
            yield return null;
            //yield return waitTime;
        }

        Debug.Log("Done Preparing Video");

        //Assign the Texture from Video to RawImage to be displayed
        image.texture = videoPlayer.texture;

        //Play Video
        videoPlayer.Play();

        //Play Sound
        audioSource.Play();

        Debug.Log("Playing Video");
        while (videoPlayer.isPlaying)
        {
            Debug.LogWarning("Video Time: " + Mathf.FloorToInt((float)videoPlayer.time));
            yield return null;
        }

        Debug.Log("Done Playing Video");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
